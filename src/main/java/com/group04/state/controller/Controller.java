package com.group04.state.controller;

import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskHolder;
import com.group04.state.model.users.*;
import com.group04.state.model.users.userImpl.DevTeamHolder;
import com.group04.state.model.users.userImpl.Developer;
import com.group04.state.model.users.userImpl.TeamLead;
import com.group04.state.model.users.userImpl.Tester;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private Pattern manualPattern = Pattern.compile("(?i)manual");
    private Pattern showUserTasksPattern = Pattern.compile("(?i)show\\s+user\\s+tasks");
    private Pattern showAllTasksPattern = Pattern.compile("(?i)show\\s+tasks");
    private Pattern showUsersPattern = Pattern.compile("(?i)show\\s+users");
    private Pattern refuseTaskPattern = Pattern.compile("(?i)refuse\\s+task\\s+(\\d+)");
    private Pattern switchDevPattern = Pattern.compile("(?i)switch\\s+to\\s+user\\s+(\\d+)");
    private Pattern switchTesterPattern = Pattern.compile("(?i)switch\\s+to\\s+tester");
    private Pattern switchTeamLeadPattern = Pattern.compile("(?i)switch\\s+to\\s+team\\s+lead");
    private Pattern moveToSprintPattern = Pattern.compile("(?i)move\\s+to\\s+sprint\\s+(\\d+)");
    private Pattern moveToInProgressPattern = Pattern.compile("(?i)move\\s+to\\s+in\\s+progress\\s+(\\d+)");
    private Pattern moveToPeerReviewPattern = Pattern.compile("(?i)move\\s+to\\s+peer\\s+review\\s+(\\d+)");
    private Pattern moveToInTestPattern = Pattern.compile("(?i)move\\s+to\\s+in\\s+test\\s+(\\d+)");
    private Pattern moveToDonePattern = Pattern.compile("(?i)move\\s+to\\s+done\\s+(\\d+)");
    private Pattern moveToBlockedPattern = Pattern.compile("(?i)move\\s+to\\s+blocked\\s+(\\d+)");
    private Pattern exitPattern = Pattern.compile("(?i)exit");

    private TaskHolder taskHolder = TaskHolder.getInstance();
    private User currentUser;
    private DevTeamHolder teamHolder = DevTeamHolder.getInstance();

    public Controller() {
        teamHolder.getProjectManager().generateProjectTasks();
    }

    private static Logger logger = LogManager.getLogger(Controller.class);

    public Printable execute(String command) throws IllegalArgumentException {
        Printable message = null;
        if (command.matches(manualPattern.pattern())) {
            message = () -> logger.trace(readManual());
        } else if (command.matches(exitPattern.pattern())) {
            message = () -> logger.trace("Bye.");
        } else if (command.matches(showUserTasksPattern.pattern())) {
            message = () -> logger.trace(taskHolder.getTasksFor(currentUser) + "\n");
        } else if (command.matches(showAllTasksPattern.pattern())) {
            for (Task t : taskHolder.getTasks()) {
                logger.trace(t + "\n");
            }
            message = () -> logger.trace("Done.\n");
        } else if (command.matches(showUsersPattern.pattern())) {
            message = () -> {
                for (User user : teamHolder.getAllUsers()) {
                    logger.trace(user + "\n");
                }
            };
        } else if (command.matches(refuseTaskPattern.pattern())) {
            Matcher matcher = refuseTaskPattern.matcher(command);
            if (matcher.find()) {
                currentUser.refuseTask(taskHolder.get(Integer.parseInt(matcher.group(1))));
                message = () -> logger.trace("Done.\n");
            }
        } else if (command.matches(switchDevPattern.pattern())) {
            Matcher matcher = switchDevPattern.matcher(command);
            if (matcher.find()) {
                currentUser = teamHolder.getUser(Long.parseLong(matcher.group(1)));
                message = () -> logger.trace("Done. "
                        + currentUser.getUserType() + " " + currentUser.getName() + ", you are welcome!");
            }

        } else if (command.matches(switchTesterPattern.pattern())) {
            currentUser = new Tester("Peter");

            message = () -> logger.trace("Done. "
                    + currentUser.getUserType() + " " + currentUser.getName() + ", you are welcome!");
        } else if (command.matches(switchTeamLeadPattern.pattern())) {
            currentUser = new TeamLead("Andrew");
            message = () -> logger.trace("Done. "
                    + currentUser.getUserType() + " " + currentUser.getName() + ", you are welcome!");
        } else if (command.matches(moveToInProgressPattern.pattern())) {
            Matcher matcher = moveToInProgressPattern.matcher(command);
            if (matcher.find()) {
                currentUser.moveToInProgress(taskHolder.get(Integer.parseInt(matcher.group(1))));
                message = () -> logger.trace("Done.\n");
            }
        } else if (command.matches(moveToPeerReviewPattern.pattern())) {
            Matcher matcher = moveToPeerReviewPattern.matcher(command);
            if (matcher.find()) {
                currentUser.moveToPeerReview(taskHolder.get(Integer.parseInt(matcher.group(1))));
                message = () -> logger.trace("Done.\n");
            }
        } else if (command.matches(moveToInTestPattern.pattern())) {
            Matcher matcher = moveToInTestPattern.matcher(command);
            if (matcher.find()) {
                currentUser.moveToInTest(taskHolder.get(Integer.parseInt(matcher.group(1))));
                message = () -> logger.trace("Done.\n");
            }
        } else if (command.matches(moveToDonePattern.pattern())) {
            Matcher matcher = moveToDonePattern.matcher(command);
            if (matcher.find()) {
                currentUser.moveToDone(taskHolder.get(Integer.parseInt(matcher.group(1))));
                message = () -> logger.trace("Done.\n");
            }
        } else if (command.matches(moveToSprintPattern.pattern())) {
            Matcher matcher = moveToSprintPattern.matcher(command);
            if (matcher.find()) {
                currentUser.moveToSprint(taskHolder.get(Integer.parseInt(matcher.group(1))));
                message = () -> logger.trace("Done.\n");
            }
        } else if (command.matches(moveToBlockedPattern.pattern())) {
            Matcher matcher = moveToBlockedPattern.matcher(command);
            if (matcher.find()) {
                currentUser.moveToBlocked(taskHolder.get(Integer.parseInt(matcher.group(1))));
                message = () -> logger.trace("Done.\n");
            }
        } else {
            message = () -> logger.error("Illegal command. Repeat please.\n");
        }

        return message;
    }

    private String readManual() {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "src/main/resources/manual.txt"));
            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append('\n');
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            logger.error(e);
        }
        return stringBuilder.toString();
    }
}
