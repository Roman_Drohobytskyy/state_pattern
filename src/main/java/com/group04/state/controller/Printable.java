package com.group04.state.controller;

@FunctionalInterface
public interface Printable {
    void print();
}

