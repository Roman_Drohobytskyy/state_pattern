package com.group04.state.model.tasks.stateImpl;

import com.group04.state.model.tasks.State;
import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;

public class PeerReviewState implements State {

    @Override
    public void inProgress(Task task) {
        task.setState(TaskState.IN_PROGRESS);
        System.out.println(task + " is not good and back to state in progress");
    }

    @Override
    public void inTest(Task task) {
        task.setState(TaskState.IN_TEST);
        System.out.println(task + " is testing");
    }
}
