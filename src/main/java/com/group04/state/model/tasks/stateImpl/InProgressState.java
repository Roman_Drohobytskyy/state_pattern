package com.group04.state.model.tasks.stateImpl;

import com.group04.state.model.tasks.State;
import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;

public class InProgressState implements State {

    @Override
    public void blocked(Task task) {
        task.setState(TaskState.BLOCKED);
        System.out.println(task + "is blocked");
    }

    @Override
    public void peerReview(Task task) {
        task.setState(TaskState.PEER_REVIEW);
        System.out.println(task + " is on peer review");
    }

    @Override
    public void sprintBacklog(Task task) {
        task.setState(TaskState.SPRINT);
        System.out.println(task + " is moved back to Sprint Backlog");
    }
}
