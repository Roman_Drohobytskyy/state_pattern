package com.group04.state.model.tasks;


public interface State {

    default void productBacklog(Task task) {
        System.out.println("Not allowed to change state to  \"in product backlog\"");
    }

    default void sprintBacklog(Task tasks) {
        System.out.println("Not allowed to change state to \" in spring backlog\"");
    }

    default void inProgress(Task task) {
        System.out.println("Not allowed to change state to \"in progress\"");
    }

    default void inTest(Task task) {
        System.out.println("Not allowed to change state to \"in test\"");
    }

    default void done(Task task) {
        System.out.println("Not allowed to change state from \"done\"");
    }

    default void blocked(Task task) {
        System.out.println("Not allowed to change state from \"blocked\"");
    }

    default void peerReview(Task task) {
        System.out.println("Not allowed to change state to \"peer review\"");
    };
}
