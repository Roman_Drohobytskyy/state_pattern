package com.group04.state.model.tasks;

public enum TaskState {
    NEW,
    SPRINT,
    IN_PROGRESS,
    PEER_REVIEW,
    IN_TEST,
    DONE,
    BLOCKED;
}
