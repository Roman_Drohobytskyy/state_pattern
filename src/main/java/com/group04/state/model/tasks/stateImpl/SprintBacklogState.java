package com.group04.state.model.tasks.stateImpl;

import com.group04.state.model.tasks.State;
import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;

public class SprintBacklogState implements State {

    @Override
    public void inProgress(Task task) {
        task.setState(TaskState.IN_PROGRESS);
        System.out.println(task + " is in progress");
    }

    @Override
    public void blocked(Task task) {
        task.setState(TaskState.BLOCKED);
        System.out.println(task + " is blocked");
    }
}
