package com.group04.state.model.tasks;

import com.group04.state.model.users.User;
import com.group04.state.model.users.userImpl.Developer;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class TaskHolder {
    private static TaskHolder instance;
    private List<Task> tasks = new LinkedList<>();

    public static TaskHolder getInstance() {
        if (instance == null) {
            instance = new TaskHolder();
        }
        return instance;
    }

    private TaskHolder() {
    }

    public Task get(int index) {
        return tasks.get(index);
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public Task getTask(int i) {
        return tasks.get(i);
    }

    public List<Task> getTasksFor(User user) {
        switch (user.getUserType()) {
            case TEAM_LEAD:
                return getLeadsTasks();
            case PROJECT_MANAGER:
                return getTasks();
            case DEVELOPER:
                return getDeveloperTasks((Developer) user);
            case TESTER:
                return getTestersTasks();
            default:
                return Collections.emptyList();
        }
    }

    public List<Task> getDeveloperTasks(Developer developer) {
        if (tasks.size() > 0) {
            return tasks.stream()
                    .filter(task -> task.getDeveloper().getId().equals(developer.getId()))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    public List<Task> getTestersTasks() {
        if (tasks.size() > 0) {
            return tasks.stream()
                    .filter(task -> task.getTaskState() == TaskState.IN_TEST)
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    public List<Task> getLeadsTasks() {
        if (tasks.size() > 0) {
            return tasks.stream()
                    .filter(task -> task.getTaskState() != TaskState.IN_TEST)
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    public List<Task> getSprintTasks() {
        return tasks.stream()
                .filter(task -> task.getTaskState().equals(TaskState.SPRINT))
                .collect(Collectors.toList());
    }
}
