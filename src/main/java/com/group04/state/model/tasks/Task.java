package com.group04.state.model.tasks;

import com.group04.state.model.tasks.stateImpl.*;
import com.group04.state.model.users.userImpl.Developer;

public class Task {
    private String taskText;
    private TaskState taskState;
    private Developer developer;
    private State state;

    public Task(String taskText) {
        this.taskText = taskText;
        this.taskState = TaskState.NEW;
        this.developer = null;
        this.state = new ProductBacklogState();
    }

    public void setState(TaskState taskState) {
        this.taskState = taskState;
        if (taskState.equals(TaskState.NEW)) {
            this.state = new ProductBacklogState();
        } else if (taskState.equals(TaskState.SPRINT)) {
            this.state = new SprintBacklogState();
        } else if (taskState.equals(TaskState.IN_PROGRESS)) {
            this.state = new InProgressState();
        } else if (taskState.equals(TaskState.PEER_REVIEW)) {
            this.state = new PeerReviewState();
        } else if (taskState.equals(TaskState.IN_TEST)) {
            this.state = new InTestState();
        } else if (taskState.equals(TaskState.DONE)) {
            this.state = new DoneState();
        } else if (taskState.equals(TaskState.BLOCKED)) {
            this.state = new BlockedState();
        } else {
            System.out.println("No such state defined");
        }
    }

    public TaskState getTaskState() {
        return taskState;
    }

    Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public void inProgressRequest() {
        state.inProgress(this);
    }

    public void selectTasksForSprint() {
        state.sprintBacklog(this);
    }

    public void inTestRequest() {
        state.inTest(this);
    }

    public void blockedRequest() {
        state.blocked(this);
    }

    public void productBacklogRequest() {
        state.productBacklog(this);
    }

    public void doneRequest() {
        state.done(this);
    }

    public void peerReviewRequest() {
        state.peerReview(this);
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskText='" + taskText + '\'' +
                ", taskState=" + taskState +
                ", developer=" + developer +
                '}';
    }
}
