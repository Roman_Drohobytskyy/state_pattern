package com.group04.state.model.tasks.stateImpl;

import com.group04.state.model.tasks.State;
import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;

public class ProductBacklogState implements State {

    @Override
    public void sprintBacklog(Task task) {
        task.setState(TaskState.SPRINT);
        System.out.println(task + " added to Sprint Backlog");
    }

    @Override
    public void blocked(Task task) {
        task.setState(TaskState.BLOCKED);
        System.out.println(task + " is blocked");
    }

}
