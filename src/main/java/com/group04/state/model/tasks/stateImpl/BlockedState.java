package com.group04.state.model.tasks.stateImpl;

import com.group04.state.model.tasks.State;
import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;

public class BlockedState implements State {

    @Override
    public void productBacklog(Task task) {
        task.setState(TaskState.BLOCKED);
        System.out.println(task + " moved back to product backlog");
    }
}
