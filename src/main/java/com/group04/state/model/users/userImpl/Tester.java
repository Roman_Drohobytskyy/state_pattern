package com.group04.state.model.users.userImpl;

import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;
import com.group04.state.model.tasks.TaskHolder;
import com.group04.state.model.users.User;
import com.group04.state.model.users.UserType;

import java.util.List;

public class Tester extends User {
    public Tester(String name) {
        super(name);
        this.userType = UserType.TESTER;
    }

    public List<Task> getUsersTasks(TaskHolder tasks) {
        return tasks.getTestersTasks();
    }

    @Override
    public void moveToDone(Task task) {
            task.doneRequest();
    }

    @Override
    public void moveToInProgress(Task task) {
        if (task.getTaskState() == TaskState.IN_TEST) {
            task.inProgressRequest();
        } else System.out.println("not allowed!");
    }
}
