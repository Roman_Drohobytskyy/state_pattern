package com.group04.state.model.users.userImpl;

import com.group04.state.model.users.User;
import com.group04.state.model.users.UserType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DevTeamHolder {
    private static DevTeamHolder instance;

    private List<User> users = new ArrayList<>();
    private Long maxId;

    public static DevTeamHolder getInstance() {
        if (instance == null) {
            instance = new DevTeamHolder();
        }
        return instance;
    }

    private DevTeamHolder() {
        maxId = 0L;
        ProjectManager projectManager = new ProjectManager("Arnold");
        TeamLead teamLead = new TeamLead("Dave");
        User user = new Developer("karl");
        addAllUsers(teamLead, projectManager, user);
    }

    private void addAllUsers(User... users) {
        for (User user : users) {
            addUser(user);
        }
    }

    public void addUser(User user) {
        users.add(user);
        user.setId(maxId++);
    }

    public List<User> getAllUsers() {
        return users;
    }

    public User getUser(Long id) {
        Optional<User> optional = users
                .stream()
                .filter(dev -> dev.getId().equals(id))
                .findFirst();
        if (!optional.isPresent()) {
            throw new RuntimeException("User not found!");
        }
        return optional.get();
    }

    public TeamLead getTeamLead() {
        Optional<User> optional = users
                .stream()
                .filter(dev -> dev.getUserType().equals(UserType.TEAM_LEAD))
                .findFirst();
        if (!optional.isPresent()) {
            throw new RuntimeException("TeamLead not found!");
        }
        return (TeamLead) optional.get();
    }

    public ProjectManager getProjectManager() {
        Optional<User> optional = users
                .stream()
                .filter(dev -> dev.getUserType().equals(UserType.PROJECT_MANAGER))
                .findFirst();
        if (!optional.isPresent()) {
            throw new RuntimeException("Project Manager not found!");
        }
        return (ProjectManager) optional.get();
    }

    public List<Developer> getDevelopers() {
        return users.stream()
                .filter(user -> user.getUserType().equals(UserType.DEVELOPER))
                .map(user -> (Developer) user)
                .collect(Collectors.toList());
    }

    public List<Tester> getTesters() {
        return users.stream()
                .filter(user -> user.getUserType().equals(UserType.TESTER))
                .map(user -> (Tester) user)
                .collect(Collectors.toList());
    }
}
