package com.group04.state.model.users;

public enum UserType {
    DEVELOPER,
    PROJECT_MANAGER,
    TEAM_LEAD,
    TESTER
}
