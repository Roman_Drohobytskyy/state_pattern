package com.group04.state.model.users.userImpl;

import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;
import com.group04.state.model.tasks.TaskHolder;
import com.group04.state.model.users.User;
import com.group04.state.model.users.UserType;

import java.util.List;

public class Developer extends User {

    public Developer(String name) {
        super(name);
        this.userType = UserType.DEVELOPER;
    }

    public List<Task> getUsersTasks(TaskHolder tasks) {
        return tasks.getDeveloperTasks(this);
    }

    @Override
    public void moveToInProgress(Task task) {
        if(task.getTaskState() == TaskState.SPRINT) {
            task.inProgressRequest();
        } else {
            System.out.println("Not allowed!");
        }
        if(task.getTaskState() == TaskState.IN_PROGRESS) {
            task.setDeveloper(this);
        }
    }

    @Override
    public void moveToPeerReview(Task task) {
        task.peerReviewRequest();
    }

    @Override
    public void refuseTask(Task task) {
        task.selectTasksForSprint();
        if(task.getTaskState() == TaskState.SPRINT) {
            task.setDeveloper(null);
        }
    }
}
