package com.group04.state.model.users.userImpl;

import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskHolder;
import com.group04.state.model.users.User;
import com.group04.state.model.users.UserType;

import java.util.List;


public class ProjectManager extends User {
    private Task projectTasks;

    public ProjectManager(String name) {
        super(name);
        this.userType = UserType.PROJECT_MANAGER;
    }

    @Override
    public void moveToBlocked(Task task) {
        task.blockedRequest();
    }

    public void generateProjectTasks() {
        TaskHolder taskHolder = TaskHolder.getInstance();
        taskHolder.addTask(new Task("task3"));
        taskHolder.addTask(new Task("task4"));
        taskHolder.addTask(new Task("task0"));
        taskHolder.addTask(new Task("task1"));
        taskHolder.addTask(new Task("task2"));
        taskHolder.addTask(new Task("task6"));
        taskHolder.addTask(new Task("task7"));
        taskHolder.addTask(new Task("task8"));
    }
}
