package com.group04.state.model.users;

import com.group04.state.model.tasks.Task;

public abstract class User {
    private Long id;
    private String name;
    protected UserType userType;

    public UserType getUserType() {
        return userType;
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void moveToSprint(Task task) {
        System.out.println("Impossible to move to Sprint!");
    }

    public void moveToInProgress(Task task) {
        System.out.println("Impossible to move to In Progress!");
    }

    public void moveToPeerReview(Task task) {
        System.out.println("Impossible to move to Peer Review!");
    }

    public void moveToInTest(Task task) {
        System.out.println("Impossible to move to In Test!");
    }

    public void moveToDone(Task task) {
        System.out.println("Impossible to move to Done!");
    }

    public void moveToBlocked(Task task) {
        System.out.println("Impossible to move to Blocked!");
    }

    public void refuseTask(Task task) { System.out.println("Impossible to refuse this task!");}

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userType=" + userType +
                '}';
    }
}
