package com.group04.state.model.users.userImpl;

import com.group04.state.model.tasks.Task;
import com.group04.state.model.tasks.TaskState;
import com.group04.state.model.tasks.TaskHolder;
import com.group04.state.model.users.User;
import com.group04.state.model.users.UserType;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class TeamLead extends User {
    public TeamLead(String name) {
        super(name);
        this.userType = UserType.TEAM_LEAD;
    }

    @Override
    public void moveToSprint(Task task) {
        task.selectTasksForSprint();
    }

    @Override
    public void moveToInTest(Task task) {
        task.inTestRequest();
    }

    @Override
    public void moveToInProgress(Task task) {
        task.inProgressRequest();
        if (task.getTaskState() == TaskState.IN_PROGRESS) {
            task.setDeveloper(new Developer("karl"));
        }
    }

    @Override
    public void moveToBlocked(Task task) {
        task.blockedRequest();
    }

    public List<Task> selectTasksForSprint() {
        Random random = new Random();
        return TaskHolder.getInstance().getTasks().stream()
                .filter(task -> task.getTaskState().equals(TaskState.NEW) && random.nextBoolean())
                .collect(Collectors.toList());
    }
}
