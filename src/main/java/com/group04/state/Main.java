package com.group04.state;

import com.group04.state.controller.Controller;
import com.group04.state.view.View;

public class Main {
    public static void main(String[] args) {
        new View(new Controller()).initUI();
    }
}
